#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h> //Header file for sleep()
#include <stdlib.h>
#include <list>
#include <algorithm>
#include <semaphore.h> 
#include "process.h"
#include "cpu_scheduler.cpp"

//std::list<Process> received;
int pcount=0;
bool cpu_schedulerStarted=0;
list<Process> jobPool;


sem_t lockJobPool; 


int insertProcessToJobPool(Process proc)
{
    sem_wait(&lockJobPool); 
    proc.id=pcount;
    pcount+=1;
    proc.burst_duration=proc.burst;

    jobPool.push_back(proc);    
    sem_post(&lockJobPool);
    return proc.id;
}
Process* getAJobPoolProcess()
{
    Process *p;
    sem_wait(&lockJobPool); 
    if(jobPool.size()>0){ 
        p=&(jobPool.front());
        jobPool.pop_front();
    }
    sem_post(&lockJobPool);
    return p;
}

void *assingProcessToMemory(void *unused)
{
    Process *p ;
    sem_init(&lockJobPool, 0, 1);
    while(!stop){
        p=getAJobPoolProcess();
        if(p!=0)
            push_backProcess(p);
        sleep(2);
    }
    sem_destroy(&lockJobPool); 

}


std::string receiveProcess(Process p){
    //received.insert(*p);
    int pid=insertProcessToJobPool(p);

    std::string result="Process received pid="+to_string(pid);
    return result;
}


