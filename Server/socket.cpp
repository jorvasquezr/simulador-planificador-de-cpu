//
// Created by jorvasquez on 16/10/20.
//
#include "socket.h"
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include "msgInterpreter.cpp"
#define PORT 8080



int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};




int startSocket(pthread_t thread_idsocket)
{
    int rc;
    long id=3;

    // Creating socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                   &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr *)&address,
             sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                             (socklen_t*)&addrlen))<0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    rc = pthread_create(&thread_idsocket, NULL, readd,(void*)id);

    
    if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        return -1;
    }
    pthread_join(thread_idsocket, NULL);
    return 0;

}


void doSomething(){
    printf("Li\n");
}
void reconnect(){
    std::cout<<"reconnecting..."<<std::endl;
    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                             (socklen_t*)&addrlen))<0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
}
void *readd(void *threadid){
    long tid;
    tid = (long)threadid;
    while(!stop){
        valread = read( new_socket , buffer, 1024);
        if(valread==0){
            reconnect();
        }
        std::string buff = buffer;
        std::string result = interpretMessage(buff);
        sendd(&result[0]);
        std::fill( std::begin( buffer ), std::end( buffer ), 0 );

    }
    pthread_exit(NULL);

}

int sendd(char *sometext){
    send(new_socket , sometext , strlen(sometext) , 0 );
    return 0;
}

