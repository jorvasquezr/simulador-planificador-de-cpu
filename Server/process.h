#ifndef PROCESS_H_
#define PROCESS_H_
struct Process
{
    int id;
    int burst;
    int burst_duration;
    int priority;
    int WT = 0;
};
#endif