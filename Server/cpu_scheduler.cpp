#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h> //Header file for sleep()
#include <stdlib.h>
#include <list>
#include <algorithm>
#include <termios.h>
#include <sys/select.h>
#include "process.h"
#include <semaphore.h> 

#define MAXCHAR 1000
#define CLOCK_CYCLE_DURATION 1 //seconds

using namespace std;

pthread_mutex_t lock; 
sem_t mutex; 
bool stop=0;

std::list<Process> poolQueue;
std::list<Process> readyQueue;
int currentPID=-1;





struct randomRangesFromUser
{
    int burst1;
    int burst2;
    int cRate;
};


struct randomRangesFromUser iniRanges;


//New code for keyPressed--------------------------------------------------------

//Disables the stdin "blocked check" of some input
int keyPressedSet()
{
    struct timeval tv;
    fd_set fds;

    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(0, &fds);

    return FD_ISSET(0, &fds);
}

//It turns off canonical mode
void turnOffCanonicalMode(int state)
{
    struct termios ttystate;
    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state) //turn off canonical mode
    {
        ttystate.c_lflag &= ~ICANON;
        ttystate.c_cc[VMIN] = 1; //minimum of number input read.
    }
    else //turn on canonical mode
    {
        ttystate.c_lflag |= ICANON;
    }
    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

void *keyPressedThread(void *args)
{
    char c;
    int i = 0;
    bool whileCond = true;
    list<Process>* readyQ = (list<Process>*) args;

    turnOffCanonicalMode(true);
    while (!stop)
    {
        
        i = keyPressedSet();
        if (i != 0)
        {
            fflush(stdin);
            c = fgetc(stdin);
            printf("\r");
            if (c == 'r') //This is the key for printing!
            {
     
                sem_wait(&mutex); 
                //Process: 1 Burst: 2 Priority: 5 WT: 8
                
                printf("\033[;32m_____________Ready Queue_____________\n");
                for (std::list<Process>::iterator it = readyQ->begin(); it != readyQ->end(); ++it){
                    if((*it).id!=currentPID){
                        cout << "Process: " << (*it).id << " Burst: " << (*it).burst << " Priority: " << (*it).priority << " WT: " << (*it).WT << endl;
                    }


                }
                cout<<"\033[0m"<<endl;
                sem_post(&mutex); 
            }
            if (c == 's') //Turn off this thread!
            {
                stop=true;
                cout << "\033[;32m Ending key pressed thread \033[0m \n";
            }

        }
        usleep(10000);
    }
    turnOffCanonicalMode(false);

    return NULL;
}



//New code for keyPressed--------------------------------------------------------

class planningAlgorithm
{
public:
    bool preemptive;
    bool isPreemptive() { return preemptive; }
    virtual Process *getProcess(list<Process> *ready) { return &ready->front(); }
    virtual void addProcess(list<Process> *ready, Process process) = 0;
    virtual void addCurrentProcess(list<Process> *ready, Process process) = 0;
    virtual void order(list<Process> *ready) = 0;
};

struct scheduler_params
{
    planningAlgorithm *planner;
    list<Process> *ready;
    bool *process_terminated;
};

struct scheduler_params params;

class FIFO : public planningAlgorithm
{
public:
    FIFO() { preemptive = false; }

    void addProcess(list<Process> *ready, Process process)
    {
        ready->insert(upper_bound(ready->begin(), ready->end(), process.id, [](int rhs, const Process &lhs) { return rhs < lhs.id; }), process);
    }
    void addCurrentProcess(list<Process> *ready, Process process) { addProcess(ready, process); }
    void order(list<Process> *ready)
    {
        ready->sort([](const Process &f, const Process &s) { return f.id < s.id; });
    }
};

class SJF : public planningAlgorithm
{
public:
    SJF(bool isPreemptive) { preemptive = isPreemptive; }

    void addProcess(list<Process> *ready, Process process)
    {
        ready->insert(upper_bound(ready->begin(), ready->end(), process.burst, [](int rhs, const Process &lhs) { return rhs < lhs.burst; }), process);
    }

    void addCurrentProcess(list<Process> *ready, Process process)
    {
        ready->insert(lower_bound(ready->begin(), ready->end(), process.burst, [](const Process &lhs, int rhs) { return lhs.burst < rhs; }), process);
    }

    void order(list<Process> *ready)
    {
        ready->sort([](const Process &f, const Process &s) { return f.burst < s.burst; });
    }
};

class HPF : public planningAlgorithm
{
public:
    HPF(bool isPreemptive) { preemptive = isPreemptive; }

    void addProcess(list<Process> *ready, Process process)
    {
        ready->insert(upper_bound(ready->begin(), ready->end(), process.priority, [](int rhs, const Process &lhs) { return rhs < lhs.priority; }), process);
    }

    void addCurrentProcess(list<Process> *ready, Process process)
    {
        ready->insert(lower_bound(ready->begin(), ready->end(), process.priority, [](const Process &lhs, int rhs) { return lhs.priority < rhs; }), process);
    }

    void order(list<Process> *ready)
    {
        ready->sort([](const Process &f, const Process &s) { return f.priority < s.priority; });
    }
};

class RR : public planningAlgorithm
{
public:
    int quantum_duration;
    int current_quantum;
    bool *process_Terminated;

    RR(int _quantum_duration, bool *_process_Terminated)
    {
        quantum_duration = _quantum_duration;
        current_quantum = _quantum_duration;
        preemptive = true;
        process_Terminated = _process_Terminated;
    }

    void restartQuantum() { current_quantum = quantum_duration; }
    Process *getProcess(list<Process> *ready)
    {
        if (*process_Terminated)
        {
            restartQuantum();
        }
        current_quantum--;
        return planningAlgorithm::getProcess(ready);
    }
    void addProcess(list<Process> *ready, Process process)
    {
        ready->push_back(process);
    }
    void addCurrentProcess(list<Process> *ready, Process process)
    {
        if (current_quantum == 0)
        {
            restartQuantum();
            addProcess(ready, process);
        }
        else
        {
            ready->push_front(process);
        }
    }
    void order(list<Process> *ready)
    {
        ready->sort([](const Process &f, const Process &s) { return f.id < s.id; });
    }
};

void increase_WT(list<Process> *ready, Process *current)
{
    for (auto it = ready->begin(); it != ready->end(); ++it)
    {
        if (current->id != (*it).id)
            (*it).WT += CLOCK_CYCLE_DURATION;
    }
}

planningAlgorithm *getAlgorithm()
{
    int alg;
    std::cout<<
"\t._______________________.\n"<<
"\t|░░░░░░░░░░░░▄░░▄░▀█▄░░░|\n"<<
"\t|░░▄████████▄██▄██▄██░░░|\n"<<
"\t|░░█████████████▄████▌░░|\n"<<
"\t|░░▌████████████▀▀▀▀▀░░░|\n"<<
"\t|▒▀▒▐█▄▐█▄▐█▄▐█▄▒░▒░▒░▒▒|\n"<<
"\t|CPU SCHEDULER SIMULATOR|\n"<< std::endl;
    cout << "Choose algorithm: (Type the number) " << endl;
    cout << "   1. First In First Out (FIFO) " << endl;
    cout << "   2. Shortest Job First (SFJ) " << endl;
    cout << "   3. Highest Priority First (HPF) " << endl;
    cout << "   4. Round Robin (RR) " << endl
         << endl;
    while (true)
    {
        cout << "Choose algorithm (Type the number): ";
        cin >> alg;
        if (alg > 0 && alg < 5)
        {
            if (alg == 1)
                return new FIFO();
            else if (alg == 4)
            {
                int quantum;
                cout << "Quantum duration (seconds): ";
                cin >> quantum;
                if (quantum > 0)
                    return new RR(quantum, params.process_terminated);
            }
            else
            {
                string yn;
                cout << "Is preemptive (y or n) ?: ";
                cin >> yn;
                if (yn == "y")
                {
                    if (alg == 2)
                        return new SJF(true);
                    if (alg == 3)
                        return new HPF(true);
                }
                else if (yn == "n")
                {
                    if (alg == 2)
                        return new SJF(false);
                    if (alg == 3)
                        return new HPF(false);
                }
            }
        }
    }
}

void showTerminated(list<Process> *terminated, int cpu_time, int cpu_idle_time ){
    
    std::cout<< "\033[;34m____________Execution Summary____________" << std::endl;
    std::cout<< "CPU time: " << cpu_time << std::endl;
    std::cout<< "CPU idle time: " << cpu_idle_time << std::endl;
    int number_Process_Executed=terminated->size();
    std::cout<< "Number of processes executed: "<<number_Process_Executed<<std::endl;
    if(number_Process_Executed>0){
        std::cout<< "Finished Processes:"<<std::endl;
        int totalTurnAroundTime=0;
        int totalWaitingTime=0;
        for (std::list<Process>::iterator it = terminated->begin(); it != terminated->end(); ++it){
            cout << "\tProcess: " << (*it).id << " Turn Around Time: " << ((*it).burst_duration+(*it).WT) << " Waiting Time: " << (*it).WT << endl;
            totalWaitingTime+= (*it).WT;
            totalTurnAroundTime+=((*it).burst_duration+(*it).WT) ;
        }
        float averageTurnAroundTime = totalTurnAroundTime / number_Process_Executed;
        float averageWaitingTime= totalWaitingTime / number_Process_Executed;
        std::cout<< "Average Turn Around Time: "<<averageTurnAroundTime <<std::endl;
        std::cout<< "Average Waiting Time: "<< averageWaitingTime<<std::endl;
    }
    std::cout<<"\033[0m"<<std::endl;
}

void *clock_cycle(void *args)

{
    int cpu_time = 0;
    int cpu_idle_time = 0; //seconds
    bool runningEmulation = false;
    
    list<Process> terminated;
    Process *current = nullptr;

    bool *process_Terminated = params.process_terminated;
    planningAlgorithm *planner = params.planner;
    list<Process> *ready = (params.ready);

    while (!stop)
    {
        if(!ready->empty() && !runningEmulation){
            std::cout<< "start simulation"<< std::endl;
            runningEmulation=true;
        }
        if(runningEmulation){
            sem_wait(&mutex); 
            if (current == nullptr && ready->empty())
            {
                cpu_idle_time++;
            }
            else
            {
                
                if (current == nullptr)
                {

                    current = planner->getProcess(ready);
                    currentPID=current->id;
                    *process_Terminated = false;
                }
                else
                {
                    

                    if (planner->isPreemptive())
                    {
                        Process *last = current;
                        ready->remove_if([current](Process p) { return p.id == current->id; });
                        planner->addCurrentProcess(ready, *current);
                        current = planner->getProcess(ready);
                        if(last->id != current->id){
                            std::cout<<"Context Switch :\n";
                            std::cout<< "From PID: " << last->id <<" to PID: " << current->id << " at "<< cpu_time <<std::endl;
                        }
                    }
                    currentPID=current->id;
                }

                (*current).burst--;
                increase_WT(ready, current);

                if (current->burst <= 0)
                {
                    cout << "\033[;36mProcess " << (*current).id << " has terminated his duty with us \033[0m" << endl;
                    ready->remove_if([current](Process p) { return p.id == current->id; });
                    terminated.push_back(*current);
                    current = nullptr;
                    *process_Terminated = true;
                }
            }

            cpu_time++;
            sem_post(&mutex); 
        }
        sleep(CLOCK_CYCLE_DURATION); 
    }
    showTerminated(&terminated,cpu_time,cpu_idle_time);

}
planningAlgorithm *algorithmA;

void push_backProcess(Process *p){
    sem_wait(&mutex); 
    algorithmA->addProcess(&readyQueue, *p);
    sem_post(&mutex); 
}



void *startCPU_S(void *args)
{
    sem_init(&mutex, 0, 1); 
    params.ready = &readyQueue;
    bool process_terminated;
    params.process_terminated = &process_terminated;
    algorithmA = getAlgorithm();
    params.planner = algorithmA;

    srand(time(NULL));

    iniRanges.burst1 = 1;
    iniRanges.burst2 = 5;

    //pthread_t thread_id_pc;
    pthread_t thread_id_cs;
    pthread_t thread_id_kp;

    //pthread_create(&thread_id_pc, NULL, processGenerator, ready);
    pthread_create(&thread_id_cs, NULL, clock_cycle, &params);
    pthread_create(&thread_id_kp, NULL, keyPressedThread, &readyQueue);

    //pthread_join(thread_id_pc, NULL);
    pthread_join(thread_id_cs, NULL);
    pthread_join(thread_id_kp, NULL);

    sem_destroy(&mutex); 
}
