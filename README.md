

## Simulador Planificador de CPU

### Instituto Tecnológico de Costa Rica
#### Principios de Sistemas Operativos
##### Prof. Erika Marín Shumann



##### Estudiantes
- Jorge Arturo Vázquez       2018102354
- David Salazar              2018100227
- Kendall Tencio Esquivel    2018107582

##### Descripción
Se tiene como finalidad el desarrollo un simulador de planificador de CPU. El cual implementa los siguientes algoritmos: FIFO, SJF, HPF y Round Robin con un quantum especificado por el usuario. Además, este proyecto utiliza un esquema cliente servidor que el cual envia mediante sockets la información de los procesos a ejecutar.
Se utiliza un cliente que funciona de 2 maneras: automático o manual. El modo manual lee desde un archivo la información de procesos (PID, burst, prioridad). El modo automático por su parte crea un proceso con estos valores al azar. Por cada proceso se crea un hilo el cual es conectado via sockets al planificador de CPU. Este podrá estar en otra computadora, oyendo desde algún puerto definido.
El planificador de CPU recibe la información de los procesos, los pone en una cola de espera para empezar la “ejecución” de los procesos. Apenas se tenga un proceso en la cola de espera ya se iniciará la simulación de la ejecución del mismo. El Planificador de CPU puede tener al menos 2 hilos: uno que reciba los mensajes del socket y ponga los procesos en la cola (Job scheduler) y otro que seleccione cual proceso tendrá el CPU (Cpu scheduler) y simule al CPU ejecutando. La ejecución de un proceso debe ser de la duración real del burst.
El proyecto fue desarrollado en C para Linux, utilizando la librería pthreads