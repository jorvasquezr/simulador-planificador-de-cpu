//
// Created by jorvasquez on 16/10/20.
//

// Client side C/C++ program to demonstrate Socket programming
#include "socketClient.h"
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#define PORT 8080
int sock = 0, valread;
struct sockaddr_in serv_addr;
char *hello = "Hello from client";
char buffer[1024] = {0};
bool stop=false;



int startSocket( )
{



    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    /*
    while(!stopped){
        std::string str;
        std::cin.clear();
        std::getline( std::cin, str );
        char *cstr = &str[0];
        sendd(cstr);
    }
    */
    return 0;
}



std::string readd(){
    std::fill( std::begin( buffer ), std::end( buffer ), 0 );
    valread = read( sock , buffer, 1024);
    if(valread ==0){
        stop=true;
        return " Error not received";
    }
    std::string result=buffer;
    return result;
}
int sendd(std::string sometext){
    char *s  = &sometext[0];
    send(sock , s , strlen(s) , 0 );
    return 0;
}

