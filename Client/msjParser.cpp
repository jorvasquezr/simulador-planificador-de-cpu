#include <string>
#include <iostream>
#include <json/json.h>
#include "process.h"

using namespace std;

string convertProccessToStringJson(process *arg){
    string command = "";
    string burst = to_string(arg->burst);
    string priority = to_string(arg ->priority);

    Json::FastWriter fastWriter;
    Json::Value newValue;

    newValue["command"] = "newProcess";
    newValue["burst"] = arg->burst;
    newValue["priority"] = arg ->priority;

    string msj= fastWriter.write(newValue);
    return msj;
}

Json::Value parseStringToJson(string msj){
  Json::Reader reader;
  Json::Value root;
  bool parseSuccess = reader.parse(msj, root, false);
 /* if (parseSuccess)
  {
    const Json::Value resultValue = root["burst"];
    cout << "Result is " << resultValue.asString() << "\n";
  }
  */
  if (parseSuccess){
      return root;
  }
  return NULL;
}

