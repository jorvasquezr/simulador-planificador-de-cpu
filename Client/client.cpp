#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h> //Header file for sleep()
#include <stdlib.h>
#include "msjParser.cpp"
#include "socketClient.cpp"

#define MAXCHAR 1000

using namespace std;

struct randomRangesFromUser
{
    int burst1;
    int burst2;
    int cRate;
};
struct randomRangesFromUser iniRanges;

//Function declarations
void processGenerator();
void readProcessTxtFile();
void threadProcessCreation(struct Process);
void generateProcess(int i);

//This place is for the threads' structure for each process read in the text file.
void *processThread(void *args)
{
    process *argsStruct = (process *)args;
    process newProc = *argsStruct;

    cout << "  Burst: " << newProc.burst << "\n";
    cout << "  Priority: " << newProc.priority << "\n";
    cout << "Waiting..."
         << "\n";
    sleep(2); //The thread waits for 2 seconds and sends the information
    cout << "Sending Information\n"
         << "\n";
    string msj = convertProccessToStringJson(argsStruct);
    sendd(msj);
    string result = readd();

    std::cout << result << std::endl;

    int randNum = rand() % 6 + 3;
    cout << "Waiting for next process..."
         << "\n";

    sleep(randNum); //Sleep time between the reading of the file and the process creation
    return NULL;
}

void autoClientMode()
{
    cout << "BURST: \n";
    cout << "First value for the random range: ";
    cin >> iniRanges.burst1;
    cout << "Second value for the random range: ";
    cin >> iniRanges.burst2;

    if (iniRanges.burst1 >= iniRanges.burst2 || iniRanges.burst1 <= 0 || iniRanges.burst2 <= 0)
    {
        cout << "\nError in range | Burst\n";
    }
    else
    {
        cout << "How many processes do you want to generate? \n";
        cout << "Insert the value here: ";
        cin >> iniRanges.cRate;

        cout << "Burst Random: [" << iniRanges.burst1 << ", " << iniRanges.burst2 << "]\n";
        cout << "C. Rate     : [" << iniRanges.cRate << "]\n";

        processGenerator();
    }
}

void manClientMode()
{
    readProcessTxtFile();
}

void readProcessTxtFile()
{
    ifstream file("processes.txt");
    string str;
    while (getline(file, str))
    {
        struct Process newProc;

        istringstream buf(str);
        istream_iterator<string> beg(buf), end;
        vector<string> processTxt(beg, end);

        newProc.burst = stoi(processTxt[0]);
        newProc.priority = stoi(processTxt[1]);

        if (newProc.burst > 0 && newProc.priority > 0)
        {
            threadProcessCreation(newProc); //Sends the info to a thread
        }
        else
        {
            cout << "ERROR - Process's burst and priority -> " << newProc.burst << ", " << newProc.priority << "\n";
        }
    }
}

void generateProcess(int i){
    int randBurst, randPriority;
    struct Process newProc;

    randBurst = rand() % (iniRanges.burst2+1-iniRanges.burst1) +iniRanges.burst1 ;

    randPriority = rand() % 5 + 1;

    newProc.burst = randBurst;
    newProc.priority = randPriority;
    cout << "\nProcess " << i << "\n";


    threadProcessCreation(newProc); //Sends the info to a thread
}

void processGenerator()
{

    if(iniRanges.cRate==-1){
        int i = 0;
        while(!stop){
            generateProcess(i);
            i+=1;
            }
    }else{
        for (int i = 0; i < iniRanges.cRate; i++)
            generateProcess(i);

        
    }
}


void threadProcessCreation(struct Process newProc)
{
    pthread_t thread_id;
    cout << "New process:"
         << "\n";

    pthread_create(&thread_id, NULL, processThread, &newProc);

    pthread_join(thread_id, NULL);
    cout << "End\n"
         << "\n";
}

int main()
{
    srand(time(NULL));
    startSocket();
    int  randBurst;

    int clientMode;
    while (!stop)
    {
        cout << "Select the Client Mode: \nAutomatic: 1 \nManual: 2\n";
        cout << "Insert value: ";
        cin >> clientMode;
        if (clientMode == 1){
            autoClientMode();
            stop=true;
            }
        else if (clientMode == 2){
            manClientMode();
            stop=true;
            }
        else
            cout << "Invalid selection...\n\n";
    }
    return 0;
}